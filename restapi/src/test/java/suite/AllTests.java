package suite;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import tests.ProjectTests;
import tests.ResponseCodeTests;

@RunWith(Suite.class)
@Suite.SuiteClasses({
        ProjectTests.class,
        ResponseCodeTests.class
})

public class AllTests {
    /* tests done for rest: https://docs.github.com/en/free-pro-team@latest/rest/reference/projects */
}