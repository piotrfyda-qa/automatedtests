package tests;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.HttpEntity;
import org.apache.http.HttpHeaders;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpPatch;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.junit.Assert;
import org.junit.Test;
import testdata.ProjectData;
import testframework.Credentials;
import testframework.util.HttpRequestPreparationUtils;
import testframework.util.ProjectUtils;

import java.io.IOException;

import static testframework.util.HttpRequestPreparationUtils.ACCEPT_HEADER_NAME;
import static testframework.util.HttpRequestPreparationUtils.ACCEPT_HEADER_VALUE;

public class ResponseCodeTests extends BaseTestClass {
    private HttpEntity body;
    private int actualStatusCode;

    @Test
    public void When_InvalidStructureProvidedMeansInvalidTag_Then_Code422UnprocessableEntityReturned() throws IOException {
        actualStatusCode = sendPostRequestAndGetResponseCode(ProjectData.JSON_PROJECT_DATA_INVALID_TAG);

        Assert.assertEquals(422, actualStatusCode);
    }

    @Test
    public void When_InvalidDataFormatProvidedMeansIntInsteadOfStringInNameTag_Then_Code422UnprocessableEntityReturned() throws IOException {
        actualStatusCode = sendPostRequestAndGetResponseCode(ProjectData.JSON_PROJECT_DATA_INVALID_FORMAT_IN_TAG_NAME);

        Assert.assertEquals(422, actualStatusCode);
    }

    @Test
    public void When_InvalidDataProvidedMeansNotAllowedStateType_Then_Code422UnprocessableEntityReturned() throws IOException {
        /* information: state 'rejected' is not allowed. Only 'open' and 'close' is valid */
        testframework.entities.Project projectDataWithInvalidStateValue = ProjectUtils.prepareProjectData("newName", "newBody", "rejected");

        ObjectMapper mapper = new ObjectMapper();
        String jsonProjectData = mapper.writeValueAsString(projectDataWithInvalidStateValue);

        HttpPatch request = HttpRequestPreparationUtils.prepareHttpPatchWithAuthentication("/projects/" + ProjectData.PROJECT_ID_FOR_UPDATE);

        body = new StringEntity(jsonProjectData);
        request.setEntity(body);
        response = client.execute(request);

        actualStatusCode = response.getStatusLine().getStatusCode();
        Assert.assertEquals(422, actualStatusCode);
    }

    @Test
    public void When_InvalidTokenProvided_Then_Code401UnauthorizeReturned() throws IOException {
        HttpDelete request = new HttpDelete(HttpRequestPreparationUtils.BASE_ENDPOINT + "/projects/" + ProjectData.PROJECT_ID_FOR_DELETION);
        request.setHeader(ACCEPT_HEADER_NAME, ACCEPT_HEADER_VALUE);
        request.setHeader(HttpHeaders.AUTHORIZATION, "token " + Credentials.INVALID_TOKEN);

        response = client.execute(request);
        actualStatusCode = response.getStatusLine().getStatusCode();
        Assert.assertEquals(401, actualStatusCode);

    }

    private int sendPostRequestAndGetResponseCode(String jsonProjectData) throws IOException {
        HttpPost postRequest = HttpRequestPreparationUtils.prepareHttpPostWithAuthentication(PROJECTS_PATH);

        body = new StringEntity(jsonProjectData);
        postRequest.setEntity(body);
        response = client.execute(postRequest);

        return response.getStatusLine().getStatusCode();
    }

}