package tests;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPatch;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.junit.Assert;
import org.junit.Test;
import testdata.ProjectData;
import testframework.entities.Project;
import testframework.util.HttpRequestPreparationUtils;
import testframework.util.ProjectUtils;
import testframework.util.ResponseUtils;

import java.io.IOException;
import java.util.List;

import static testframework.util.ProjectUtils.getProjectById;
import static testframework.util.ProjectUtils.getProjectByName;

public class ProjectTests extends BaseTestClass {

    private final ObjectMapper mapper = new ObjectMapper();
    private Project projectData;
    private List<Project> projectList;
    private String jsonProjectData;
    private int actualStatusCode;
    private HttpEntity body;

    @Test
    public void When_ProperCreationProjectDataProvided_Then_ProjectCreationFinishedSuccessfully() throws IOException {
        String projectName = "project" + System.currentTimeMillis();
        String projectBody = "rest api automated project";
        Project projectAfterCreation;

        projectData = ProjectUtils.prepareProjectData(projectName, projectBody, null);
        jsonProjectData = mapper.writeValueAsString(projectData);

        HttpPost request = HttpRequestPreparationUtils.prepareHttpPostWithAuthentication(PROJECTS_PATH);
        body = new StringEntity(jsonProjectData);
        request.setEntity(body);
        response = client.execute(request);

        actualStatusCode = response.getStatusLine().getStatusCode();
        Assert.assertEquals("Response code is different than creation confirmation.", 201, actualStatusCode);

        projectAfterCreation = getProjectByName(projectName, getAllExistingProjects());

        if (projectAfterCreation == null) {
            Assert.fail("Project by name not found");
        } else {
            Assert.assertEquals("Project name is different than expected.", projectName, projectAfterCreation.getName());
            Assert.assertEquals("Project body is different than expected.", projectBody, projectAfterCreation.getBody());

            System.out.println("Created: project name: " + projectName);
        }
    }

    @Test
    public void When_ProperUpdateProjectDataProvided_Then_ProjectUpdateFinishedSuccessfully() throws IOException {
        String projectName = "edited" + System.currentTimeMillis();
        String projectBody = "rest api automated project edited";
        Project projectAfterEdition;

        Assert.assertNotNull("Project for update doesn't exist", getProjectById(ProjectData.PROJECT_ID_FOR_UPDATE, getAllExistingProjects()));

        projectData = ProjectUtils.prepareProjectData(projectName, projectBody, null);
        jsonProjectData = mapper.writeValueAsString(projectData);

        HttpPatch request = HttpRequestPreparationUtils.prepareHttpPatchWithAuthentication("/projects/" + ProjectData.PROJECT_ID_FOR_UPDATE);
        body = new StringEntity(jsonProjectData);
        request.setEntity(body);
        response = client.execute(request);

        actualStatusCode = response.getStatusLine().getStatusCode();
        Assert.assertEquals("Request finished with error.", 200, actualStatusCode);

        projectAfterEdition = getProjectById(ProjectData.PROJECT_ID_FOR_UPDATE, getAllExistingProjects());
        if (projectAfterEdition == null) {
            Assert.fail("Project by id not found");
        } else {
            Assert.assertEquals("Project name is different than expected.", projectName, projectAfterEdition.getName());
            Assert.assertEquals("Project body is different than expected.", projectBody, projectAfterEdition.getBody());
        }
    }

    @Test
    public void When_ProjectDeletionCalledById_Then_ProjectDeletionFinishedSuccessfully() throws IOException {

        projectList = getAllExistingProjects();
        int listSize = projectList.size();
        int highestProjectId = projectList.get(listSize - 1).getId();

        Assert.assertNotNull("Project for deletion doesn't exist. Project id: " + highestProjectId, getProjectById(highestProjectId, getAllExistingProjects()));

        HttpDelete request = HttpRequestPreparationUtils.prepareHttpDeleteWithAuthentication("/projects/" + highestProjectId);
        response = client.execute(request);
        actualStatusCode = response.getStatusLine().getStatusCode();

        Assert.assertEquals("Response code is different than removal confirmation.", 204, actualStatusCode);
        Assert.assertNull("Project was not removed. Project id: " + highestProjectId, getProjectById(highestProjectId, getAllExistingProjects()));
    }

    @Test
    public void getAllExistingProjectsAndPrintItInConsole() throws IOException {

        getAllExistingProjects();

        for (Project project : projectList) {
            System.out.println("name: " + project.getName() + ", project id: " + project.getId());
        }
    }

    private List<Project> getAllExistingProjects() throws IOException {
        HttpGet get = HttpRequestPreparationUtils.prepareHttpGetWithAuthentication(PROJECTS_PATH);
        response = client.execute(get);
        projectList = ResponseUtils.unmarshall(response, Project.class);
        return projectList;
    }

}
