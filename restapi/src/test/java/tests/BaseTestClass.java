package tests;

import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.impl.client.CloseableHttpClient;
import org.junit.After;
import org.junit.Before;

import java.io.IOException;

import static testframework.infrastructure.HttpFactory.setHttpClient;

public class BaseTestClass {

    protected static final String ORGANIZATION_NAME = "qaorganization2020";
    protected static final String PROJECTS_PATH = "/orgs/" + ORGANIZATION_NAME + "/projects";

    CloseableHttpClient client;
    CloseableHttpResponse response;

    @Before
    public void setup() {
        client = setHttpClient();
    }

    @After
    public void closeResources() throws IOException {
        client.close();
        response.close();
    }

}
