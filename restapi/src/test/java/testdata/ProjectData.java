package testdata;

public class ProjectData {
    public static final Integer PROJECT_ID_FOR_UPDATE = 5948526;
    public static final Integer PROJECT_ID_FOR_DELETION = 5948754;

    public static final String JSON_PROJECT_DATA_INVALID_TAG = "{\"invalidTag\":\"Automated3\",\"body\":\"Created by rest\"}";
    public static final String JSON_PROJECT_DATA_INVALID_FORMAT_IN_TAG_NAME = "{\"name\":12345,\"body\":\"Created by rest\"}";
}