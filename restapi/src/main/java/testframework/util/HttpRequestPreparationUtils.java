package testframework.util;

import org.apache.http.HttpHeaders;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPatch;
import org.apache.http.client.methods.HttpPost;
import testframework.Credentials;

public class HttpRequestPreparationUtils {

    public static final String BASE_ENDPOINT = "https://api.github.com";

    public static final String ACCEPT_HEADER_NAME = "Accept: ";
    public static final String ACCEPT_HEADER_VALUE = "application/vnd.github.inertia-preview+json";

    public static HttpGet prepareHttpGetWithAuthentication(String path) {
        HttpGet request = new HttpGet(BASE_ENDPOINT + path);
        request.setHeader(ACCEPT_HEADER_NAME, ACCEPT_HEADER_VALUE);
        request.setHeader(HttpHeaders.AUTHORIZATION, "token " + Credentials.TOKEN);

        return request;
    }

    public static HttpPost prepareHttpPostWithAuthentication(String path) {
        HttpPost request = new HttpPost(BASE_ENDPOINT + path);
        request.setHeader(ACCEPT_HEADER_NAME, ACCEPT_HEADER_VALUE);
        request.setHeader(HttpHeaders.AUTHORIZATION, "token " + Credentials.TOKEN);

        return request;
    }

    public static HttpPatch prepareHttpPatchWithAuthentication(String path) {
        HttpPatch request = new HttpPatch(BASE_ENDPOINT + path);
        request.setHeader(ACCEPT_HEADER_NAME, ACCEPT_HEADER_VALUE);
        request.setHeader(HttpHeaders.AUTHORIZATION, "token " + Credentials.TOKEN);

        return request;
    }

    public static HttpDelete prepareHttpDeleteWithAuthentication(String path) {
        HttpDelete request = new HttpDelete(BASE_ENDPOINT + path);
        request.setHeader(ACCEPT_HEADER_NAME, ACCEPT_HEADER_VALUE);
        request.setHeader(HttpHeaders.AUTHORIZATION, "token " + Credentials.TOKEN);

        return request;
    }

}
