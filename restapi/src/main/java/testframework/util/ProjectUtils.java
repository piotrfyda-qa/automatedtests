package testframework.util;

import testframework.entities.Project;

import java.util.List;

public class ProjectUtils {

    public static Project prepareProjectData(String name, String body, String state) {
        Project proj = new Project();

        proj.setName(name);
        proj.setBody(body);

        if (state != null) {
            proj.setState(state);
        }

        return proj;
    }

    public static Project getProjectByName(String name, List<Project> projectList) {
        Project projectByName = null;

        for (Project project : projectList) {
            if (project.getName().equals(name)) {
                projectByName = project;
            }
        }

        return projectByName;
    }

    public static Project getProjectById(int id, List<Project> projectList) {
        Project projectById = null;

        for (Project project : projectList) {
            if (project.getId().equals(id)) {
                projectById = project;
            }
        }

        return projectById;
    }

}