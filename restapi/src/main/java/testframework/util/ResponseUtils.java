package testframework.util;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.TypeFactory;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.util.List;

public class ResponseUtils {

    public static <T> List<T> unmarshall(CloseableHttpResponse response, Class<T> clazz) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        String jsonBody = EntityUtils.toString(response.getEntity());

        return mapper.readValue(jsonBody, TypeFactory.defaultInstance().constructCollectionType(List.class, clazz));
    }
}