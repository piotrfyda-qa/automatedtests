package testframework.infrastructure;

import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;

public class HttpFactory {

    public static CloseableHttpClient setHttpClient() {
        return HttpClientBuilder.create().build();
    }

}
