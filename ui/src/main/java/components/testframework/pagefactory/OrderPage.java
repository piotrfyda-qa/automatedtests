package components.testframework.pagefactory;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.WebDriverWait;

public class OrderPage extends BasePage {

    @FindBy(linkText = "Proceed to checkout")
    WebElement proceedToCheckoutButtonInSummaryStage;

    @FindBy(css = "button.button:nth-child(4)")
    WebElement proceedToCheckoutButton;

    @FindBy(css = ".page-heading")
    WebElement orderStep;

    @FindBy(id = "cgv")
    WebElement termsAgreement;

    @FindBy(css = ".cheque")
    WebElement paymentTypeCheck;

    @FindBy(css = "button.button-medium > span:nth-child(1)")
    WebElement orderConfirmationButton;

    @FindBy(css = ".alert")
    WebElement orderResult;

    @FindBy(partialLinkText = "Cart")
    WebElement cartButton;

    public OrderPage(WebDriver driver, WebDriverWait wait) {
        super(driver, wait);
    }

    public void goToCart() {
        goToHomePage();
        cartButton.click();
    }

    public void goToOrderPage() {
        driver.get("http://automationpractice.com/index.php?controller=order");
    }

    public void proceedToCheckout() {
        jsScrollToWebElement(proceedToCheckoutButton);
        proceedToCheckoutButton.click();
    }

    public String getOrderStep() {
        return orderStep.getText();
    }

    public void acceptTerms() {
        termsAgreement.click();
        termsAgreement.isSelected();
    }

    public void proceedToCheckoutInSummaryStep() {
        proceedToCheckoutButtonInSummaryStage.click();
    }

    public void choosePayByCheckOption() {
        paymentTypeCheck.click();
    }

    public void confirmOrder() {
        orderConfirmationButton.click();
    }

    public String getOrderResultMessage() {
        return orderResult.getText();
    }
}