package components.testframework.pagefactory;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class LoginPage extends BasePage {

    @FindBy(linkText = "Sign in")
    WebElement signInTab;

    @FindBy(id = "email")
    WebElement emailAddressField;

    @FindBy(id = "passwd")
    WebElement passwordField;

    @FindBy(id = "SubmitLogin")
    WebElement signInButton;

    @FindBy(className = "account")
    WebElement userNameDetails;

    public LoginPage(WebDriver driver, WebDriverWait wait) {
        super(driver, wait);
    }

    public void signIn(String email, String pass) {
        signInTab.click();
        wait.until(ExpectedConditions.titleIs("Login - My Store"));
        emailAddressField.clear();
        emailAddressField.sendKeys(email);
        passwordField.clear();
        passwordField.sendKeys(pass);
        signInButton.click();
        wait.until(ExpectedConditions.titleIs("My account - My Store"));
    }

    public String getUserDetails() {
        return userNameDetails.getText();
    }
}
