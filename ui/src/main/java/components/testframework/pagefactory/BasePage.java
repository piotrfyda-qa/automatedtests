package components.testframework.pagefactory;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class BasePage {

    final String BASE_URL = "http://automationpractice.com/index.php";
    WebDriver driver;
    WebDriverWait wait;

    public BasePage(WebDriver driver, WebDriverWait wait) {
        this.driver = driver;
        this.wait = wait;

        /* this initElements method will create all WebElements */
        PageFactory.initElements(driver, this);
    }

    public void goToHomePage() {
        driver.get(BASE_URL);
        wait.until(ExpectedConditions.titleIs("My Store"));
    }

    public void jsScrollToWebElement(WebElement we) {
        ((JavascriptExecutor) driver).executeScript(
                "arguments[0].scrollIntoView(true);", we);
    }

}