package components.testframework.pagefactory;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class OrderProductSectionPage extends BasePage {

    final String cartStateByCss = ".alert";
    final String productsSectionById = "cart_summary";

    @FindBy(css = cartStateByCss)
    WebElement cartState;

    @FindBy(id = productsSectionById)
    WebElement productsSection;

    public OrderProductSectionPage(WebDriver driver, WebDriverWait wait) {
        super(driver, wait);
    }

    public String returnCartState() {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(cartStateByCss)));
        return cartState.getText();
    }

    public Boolean isProductInCart(String productName) {
        boolean isProductInCart = false;
        WebElement productsInCart;
        List<WebElement> productsList;

        try {
            productsInCart = driver.findElement(By.id(productsSectionById));
        } catch (NoSuchElementException ex) {
            return false;
        }

        productsList = productsInCart.findElements(By.xpath("tbody/tr"));

        for (WebElement product : productsList) {
            if (product.getText().contains(productName)) {
                isProductInCart = true;
            }
        }

        return isProductInCart;
    }

    public void deleteProductByName(String productName) {

        String firstPartDeletionPositionMeansTillTableCartSummary = "/html/body/div/div[2]/div/div[3]/div/div[2]/table/";
        String secondPartOfDeletionPositionMeansInTbody;
        String fullXpathDeletionButtonProductPosition;
        WebElement deletionButton;

        try {
            secondPartOfDeletionPositionMeansInTbody = getProductDeleteButtonPositionInTbody(productName);
            fullXpathDeletionButtonProductPosition = firstPartDeletionPositionMeansTillTableCartSummary + secondPartOfDeletionPositionMeansInTbody;
            deletionButton = driver.findElement(By.xpath(fullXpathDeletionButtonProductPosition));
            deletionButton.click();
            wait.until(ExpectedConditions.invisibilityOf(deletionButton));
        } catch (NullPointerException ex) {
            Assert.fail("Expected product isn't in Cart");
        }
    }

    private String getProductDeleteButtonPositionInTbody(String productName) {
        String deletionButtonPosition = null;
        List<WebElement> allProductsInCart = productsSection.findElements(By.xpath("tbody/tr"));

        if (allProductsInCart.size() == 1 && allProductsInCart.get(0).getText().contains(productName)) {
            deletionButtonPosition = "tbody/tr/td[7]/div/a/i";
        } else {
            for (int i = 1; i <= allProductsInCart.size(); i++) {
                if (allProductsInCart.get(i - 1).getText().contains(productName)) {
                    deletionButtonPosition = "tbody/tr[" + i + "]/td[7]/div/a/i";
                    break;
                }
            }
        }
        return deletionButtonPosition;
    }
}