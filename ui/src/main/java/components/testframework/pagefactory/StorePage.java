package components.testframework.pagefactory;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class StorePage extends BasePage {

    final String firstProductItemByCss = ".right-block > h5:nth-child(1) > a:nth-child(1)";
    final String addToCardById = "add_to_cart";
    final String addingResultMessageByCss = "h2:nth-child(2)";
    @FindBy(css = firstProductItemByCss)
    WebElement firstProductItem;
    @FindBy(id = addToCardById)
    WebElement addToCard;
    @FindBy(css = addingResultMessageByCss)
    WebElement addingResultMessage;

    @FindBy(id = "search_query_top")
    WebElement searchField;

    @FindBy(css = "button.btn:nth-child(5)")
    WebElement searchButton;

    @FindBy(id = "layer_cart_product_title")
    WebElement addedProductName;

    @FindBy(id = "layer_cart_product_quantity")
    WebElement addedQuantity;

    public StorePage(WebDriver driver, WebDriverWait wait) {
        super(driver, wait);
    }

    public void searchProductByName(String productName) {
        searchField.clear();
        searchField.sendKeys(productName);
        searchButton.click();

        /* wait for results rendering after search action */
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(firstProductItemByCss)));
        Assert.assertEquals("Searched product not found", productName, firstProductItem.getText());
    }

    public void addFirstProductFromResultListToCart() {
        firstProductItem.click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(addToCardById)));
        addToCard.click();
    }

    public String getAddingResultMessage() {

        for (String currentWindow : driver.getWindowHandles())
            driver.switchTo().window(currentWindow);
        {
            wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(addingResultMessageByCss)));
            return addingResultMessage.getText();
        }
    }

    public String getAddedProductName() {
        return addedProductName.getText();
    }

    public String getAddedQuantity() {
        return addedQuantity.getText();
    }

}
