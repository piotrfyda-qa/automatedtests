package components.testframework.infrastructure;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.support.ui.WebDriverWait;

public class DriverFactory {

    private static WebDriver driver;

    public static WebDriver setDriver() {
        driver = setupWebDriver();
        driver.manage().window().maximize();
        return driver;
    }

    public static WebDriverWait setWaiter() {
        return new WebDriverWait(driver, 5);
    }

    private static WebDriver setupWebDriver() {
        System.setProperty("webdriver.gecko.driver", "src/main/java/components/geckodriver028win64/geckodriver.exe");
        FirefoxProfile fp = new FirefoxProfile();
        return new FirefoxDriver(fp);
    }
}