package uitests;

import org.junit.Assert;
import org.junit.Test;
import testdata.ProductData;
import testdata.UserData;

import static testdata.MessageData.ORDER_COMPLETE_MESSAGE;
import static testdata.OrderStageData.*;
import static uitests.ProductTests.searchAndAddProductToCart;
import static uitests.SignInTests.signIn;

public class OrderTests extends BaseTestClass {

    @Test
    public void orderSpecifiedProduct() {
        final String product = ProductData.TSHIRT;

        signIn(UserData.EMAIL, UserData.PASSWORD);
        searchAndAddProductToCart(product);

        orderPage.goToOrderPage();
        Assert.assertTrue("Product isn't in the Cart: " + product, orderProductSectionPage.isProductInCart(product));

        orderPage.proceedToCheckoutInSummaryStep();
        Assert.assertEquals(ADDRESSES, orderPage.getOrderStep());

        orderPage.proceedToCheckout();
        Assert.assertEquals(SHIPPING, orderPage.getOrderStep());

        orderPage.acceptTerms();
        orderPage.proceedToCheckout();
        Assert.assertEquals(PAYMENT_METHOD, orderPage.getOrderStep());

        orderPage.choosePayByCheckOption();
        Assert.assertEquals(SUMMARY, orderPage.getOrderStep());

        orderPage.confirmOrder();
        Assert.assertEquals(ORDER_COMPLETE_MESSAGE, orderPage.getOrderResultMessage());
    }

    /* todo: extension: add more checks on each order steps eg. delivery address, total amount of order */

}
