package uitests;

import org.junit.Assert;
import org.junit.Test;
import testdata.ProductData;
import testdata.UserData;

import static testdata.MessageData.CART_EMPTY_MESSAGE;
import static testdata.MessageData.PRODUCT_ADDED_TO_CART_MESSAGE;
import static uitests.SignInTests.signIn;

public class ProductTests extends BaseTestClass {

    final String product = ProductData.TSHIRT;
    final String product2 = ProductData.BLOUSE;

    public static void searchAndAddProductToCart(String productName) {
        commonActions.goToHomePage();
        storePage.searchProductByName(productName);
        storePage.addFirstProductFromResultListToCart();
    }

    @Test
    public void addSpecifiedProductToCart() {
        signIn(UserData.EMAIL, UserData.PASSWORD);
        searchAndAddProductToCart(product);

        /* data check on confirmation pop-up */
        Assert.assertEquals(PRODUCT_ADDED_TO_CART_MESSAGE, storePage.getAddingResultMessage());
        Assert.assertEquals("Product name presented is different than added.", product, storePage.getAddedProductName());
        Assert.assertEquals("1", storePage.getAddedQuantity());

        orderPage.goToCart();
        Assert.assertTrue("Product isn't in the Cart: " + product, orderProductSectionPage.isProductInCart(product));
    }

    @Test
    public void addSpecifiedProductsToCart() {
        signIn(UserData.EMAIL, UserData.PASSWORD);
        searchAndAddProductToCart(product);
        searchAndAddProductToCart(product2);

        orderPage.goToCart();
        Assert.assertTrue("Product isn't in the Cart: " + product, orderProductSectionPage.isProductInCart(product));
        Assert.assertTrue("Product isn't in the Cart: " + product2, orderProductSectionPage.isProductInCart(product2));
    }

    @Test
    public void removeSpecifiedProductFromCart_WhenThereIsAddedOnlyOneProduct() {
        signIn(UserData.EMAIL, UserData.PASSWORD);
        searchAndAddProductToCart(product);

        orderPage.goToCart();
        Assert.assertTrue("Product specified for removal isn't in the Cart", orderProductSectionPage.isProductInCart(product));

        orderProductSectionPage.deleteProductByName(product);
        Assert.assertEquals(CART_EMPTY_MESSAGE, orderProductSectionPage.returnCartState());
        Assert.assertFalse("Product is still in the Cart - was not removed", orderProductSectionPage.isProductInCart(product));
    }

    @Test
    public void removeSpecifiedProductFromCart_WhenThereIsAddedMoreThanOneProduct() {
        signIn(UserData.EMAIL, UserData.PASSWORD);
        searchAndAddProductToCart(product);
        searchAndAddProductToCart(product2);

        orderPage.goToCart();

        orderProductSectionPage.deleteProductByName(product);
        Assert.assertFalse("Product is still in the Cart - was not removed", orderProductSectionPage.isProductInCart(product));
        Assert.assertTrue("Product isn't in the Cart - was removed but should not be", orderProductSectionPage.isProductInCart(product2));
    }

}