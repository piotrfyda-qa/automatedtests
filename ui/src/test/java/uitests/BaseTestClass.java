package uitests;

import components.testframework.pagefactory.*;
import org.junit.After;
import org.junit.Before;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import static components.testframework.infrastructure.DriverFactory.setDriver;
import static components.testframework.infrastructure.DriverFactory.setWaiter;

public class BaseTestClass {
    static WebDriver driver;
    static WebDriverWait wait;
    static LoginPage loginPage;
    static BasePage commonActions;
    static StorePage storePage;
    static OrderPage orderPage;
    static OrderProductSectionPage orderProductSectionPage;

    @Before
    public void setup() {
        driver = setDriver();
        wait = setWaiter();
        loginPage = new LoginPage(driver, wait);
        commonActions = new BasePage(driver, wait);
        storePage = new StorePage(driver, wait);
        orderPage = new OrderPage(driver, wait);
        orderProductSectionPage = new OrderProductSectionPage(driver, wait);
    }

    @After
    public void cleanUp() {
        driver.close();
    }
}
