package uitests;

import org.junit.Assert;
import org.junit.Test;
import testdata.UserData;

public class SignInTests extends BaseTestClass {

    public static void signIn(final String email, final String password) {
        commonActions.goToHomePage();
        loginPage.signIn(email, password);
    }

    @Test
    public void When_ProperCredentialsProvided_Then_UserSuccessfullySignedIn() {
        signIn(UserData.EMAIL, UserData.PASSWORD);

        Assert.assertEquals("User data is different than expected for provided login",
                UserData.FIRST_NAME + " " + UserData.LAST_NAME, loginPage.getUserDetails());
    }

    /* todo: extension: tests for negative cases eg. invalid password */

}