package testdata;

public class OrderStageData {

    public static final String ADDRESSES = "ADDRESSES";
    public static final String SHIPPING = "SHIPPING";
    public static final String PAYMENT_METHOD = "PLEASE CHOOSE YOUR PAYMENT METHOD";
    public static final String SUMMARY = "ORDER SUMMARY";

}