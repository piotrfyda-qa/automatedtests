package testdata;

public class UserData {

    public static final String FIRST_NAME = "Piotr";
    public static final String LAST_NAME = "Kowalski";

    public static final String EMAIL = "piotrfydaconsulting@gmail.com";
    public static final String PASSWORD = "TheBestPass123";
}