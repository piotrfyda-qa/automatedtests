package testdata;

public class MessageData {
    public static final String ORDER_COMPLETE_MESSAGE = "Your order on My Store is complete.";
    public static final String CART_EMPTY_MESSAGE = "Your shopping cart is empty.";
    public static final String PRODUCT_ADDED_TO_CART_MESSAGE = "Product successfully added to your shopping cart";
}
