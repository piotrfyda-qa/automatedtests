package suite;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import uitests.OrderTests;
import uitests.ProductTests;
import uitests.SignInTests;

@RunWith(Suite.class)
@Suite.SuiteClasses({
        SignInTests.class,
        ProductTests.class,
        OrderTests.class
})

public class AllTests {
    /* tests done for: http://automationpractice.com/index.php */
}